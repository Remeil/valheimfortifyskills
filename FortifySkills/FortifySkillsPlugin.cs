﻿using System;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using ServerSync;

namespace FortifySkills
{
    [BepInPlugin("net.merlyn42.fortifyskills", "FortifySkills", "1.4.3")]
    public class FortifySkillsPlugin : BaseUnityPlugin
    {
        private const string ModName = "FortifySkills";
        private const string V = "1.4.3";

        private static readonly ConfigSync configSync = new ConfigSync(ModName) { DisplayName = ModName, CurrentVersion = V, MinimumRequiredVersion = "1.0.0" };

        private ConfigEntry<T> config<T>(string group, string name, T value, ConfigDescription description, bool synchronizedSetting = true)
        {
            ConfigEntry<T> configEntry = Config.Bind(group, name, value, description);

            SyncedConfigEntry<T> syncedConfigEntry = configSync.AddConfigEntry(configEntry);
            syncedConfigEntry.SynchronizedConfig = synchronizedSetting;

            return configEntry;
        }

        private ConfigEntry<T> config<T>(string group, string name, T value, string description, bool synchronizedSetting = true) => config(group, name, value, new ConfigDescription(description), synchronizedSetting);

        public static ConfigEntry<bool> modEnabled;
        public static ConfigEntry<float> bonusRate;
        public static ConfigEntry<float> fortifyLevelRate;
        public static ConfigEntry<float> fortifyMaxRate;

        void Awake()
        {

            modEnabled = config("General",
                                     "ModEnabled",
                                     true,
                                     "Used to toggle the mod on and off.");
            bonusRate = config("Mechanics",
                "BonusXPRate",
                1.5f,
                new ConfigDescription("Used to control the rate at which the active level increases, 1=base game, 1.5=50% bonus xp awarded, 0.8=20% less xp awarded. Default:1.5",new AcceptableValueRange<float>(0.0f,10f)));

            fortifyLevelRate = config("Mechanics",
                "FortifyXPPerLevelRate",
                0.1f,
                new ConfigDescription("Used to control the rate at which the fortified skill XP increases PER LEVEL behind the active level. 0.1=Will gain 10% XP for every level behind the active level. Default:0.1", new AcceptableValueRange<float>(0.0f, 1f)));

            fortifyMaxRate = config("Mechanics",
                "FortifyMaxXPRate",
                0.8f,
                new ConfigDescription("Used to control the maximum rate of XP earned for the fortified skill. Caps FortifyXPPerLevelRate. Values less than 1 mean the fortify skill will always increase more slowly than the active level. 0.8=Will gain a max of 80% of the XP gained for the active skill. Default 0.8", new AcceptableValueRange<float>(0.0f, 2f)));


            config<int>("General", "NexusID", 172, "Nexus mod ID for updates, Don't change");

            if (modEnabled.Value)
            {
                UnityEngine.Debug.Log("Fortify Skills Mod "+ V + " Enabled");
                var harmony = new Harmony("mod.fortify_skills");
                harmony.PatchAll();
            }
            else
            {
                UnityEngine.Debug.Log("Fortify Skills Mod "+V+" Disabled");
            }
        }
    }
}
